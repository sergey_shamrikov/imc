$(function(){
// IPad/IPhone
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
	// Menu Android
	if(window.orientation!=undefined){
    var regM = /ipod|ipad|iphone/gi,
     result = ua.match(regM)
    if(!result) {
     $('.sf-menu li').each(function(){
      if($(">ul", this)[0]){
       $(">a", this).toggle(
        function(){
         return false;
        },
        function(){
         window.location.href = $(this).attr("href");
        }
       );
      } 
     })
    }
   } 
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')



$(document).ready(function(){

	// order form
	$('.log_order_form_btn').on('click',function(event){
		event.preventDefault();
		var $this = $(this),
			  parent = $this.closest('.services_item');
		$this.fadeOut();
		parent.find('.order_form').slideDown();

    setTimeout(function(){
      offset = $this.closest('.services_item').find(".order_form").offset().top;
      if($.browser.safari){
      $('body').animate( { scrollTop: offset }, 1100 );
      }else{
      $('html,body').animate( { scrollTop: offset}, 1100 );
      }
    },500);
	});

	//full_screen_box
	
	function fullScreen(){
		if($('.full_screen_box').length){
      if($(window).width()>992){
        var windowHeight = $(window).height(),
          offset = $('.full_screen_box').offset().top,
          bgPosition = "50% "+ offset +"px";
        $('.full_screen_box').height(windowHeight-offset);
        // $('.full_screen_box').css({
        //  "background-position":bgPosition
        // });  
      }
			
		}
	}
	fullScreen()
	$(window).on('resize',fullScreen);

  $(window).load(function(){
	  // isotope
	  if($('#filter_jobs_box').length){
	    $("#filter_jobs_box").isotope({
	      // options
	      itemSelector: '.jobs_item'
	    });
	  }
	  if($('#blog_box').length){
	    $("#blog_box").isotope({
	      // options
	      itemSelector: '.blog_item'
	    });
	  }
  	
  });

  // filter items on button click
  $('#filters_btn').on( 'click', 'a', function() {
    var filterValue = $(this).attr('data-filter');
    $("#filter_jobs_box").isotope({ filter: filterValue });
    $(this).addClass('active').siblings().removeClass('active');
  });



  // go up

  function goUp(){
  	var windowHeight = $(window).height(),
  		windowScroll = $(window).scrollTop();
  	if(windowScroll>windowHeight/2){
  		$('#go_up').addClass('active');
  	}
  	else{
  		$('#go_up').removeClass('active');
  	}
  }

  goUp();
  $(window).on('scroll',goUp);
  $('#go_up').on('click',function () {
    if($.browser.safari){
    $('body').animate( { scrollTop: 0 }, 1100 );
    }else{
    $('html,body').animate( { scrollTop: 0}, 1100 );
    }
    return false;
  });


  // anhore
  $('.anhor').on('click',function () {
    var elementClick = $(this).attr("href"),
        destination = $(elementClick).offset().top;
    $(this).parent().addClass('current').siblings('.sidebar_list_item').removeClass('current');
    if($.browser.safari){
    $('body').animate( { scrollTop: destination }, 1100 );
    }else{
    $('html,body').animate( { scrollTop: destination - 80}, 1100 );
    }
    return false;
  });

  // input
  if($('.input_box .input_type1').length){
    $('.input_box .input_type1').blur(function(){
      if(!$(this).val() == ""){
        $(this).addClass('active');
      }
      else{
        $(this).removeClass('active');
      }
    });  
  }
  


  // mobail menu
  $('.nav_mobail_btn').on("click",function(){
    $('.nav_mobail').toggleClass('active');
  });


  // gallery with thumbnail
  $('.thumbnail_box').on('click',".thumbnail_item>div",function(){
  	var mainImg = $(this).closest('.gallery_thumbnail').find('.gallery_img>img'),
        parent  = $(this).parent(".thumbnail_item"),
  		  src     = $(this).attr('data-src');
  	
  	mainImg.attr("src",src);
    parent.addClass('active').siblings(".thumbnail_item").removeClass('active');
  	// mainImg.animate({
  	// 	opacity:0.1
  	// },300);
  	// setTimeout(function(){
  		
  	// },300);
  	// mainImg.animate({
  	// 	opacity:1
  	// },300)
  });

  // slider
  if($('.services_slider').length){
  	$('.services_slider').each(function(){
  		$(this).owlCarousel({
		    // loop:true,
		    nav:true,
		    responsive:{
		        0:{
		            items:1
		        }
		    }
	  	})	
  	});
	  
  }


});

